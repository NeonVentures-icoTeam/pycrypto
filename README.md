Kinetic Crypto
==========
PyCrypto is written in Python, and built on the [Flask Framework](http://flask.pocoo.org/).    

## Dependencies
This project has a few dependencies besides those required by Flask.   

- Py Stellar Base [https://github.com/StellarCN/py-stellar-base](https://github.com/StellarCN/py-stellar-base)
