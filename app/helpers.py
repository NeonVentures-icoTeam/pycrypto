from .exceptions import StellarError
import run
from stellar_base.keypair import Keypair
from stellar_base.horizon import horizon_livenet, horizon_testnet


def get_app_config():
    """Returns the application configuration.
    
    :return:
    """
    return run.app.config


def get_db_connection_string():
    """Returns the connection string to be used for the database backend.

    The settings are placed in the .env file, and loaded into the Flask config.
    """
    app = run.app
    return '{engine}+{driver}://{user}:{password}@{host}:{port}/{schema}'.format(engine=app.config['DB_ENGINE'],
                                                                                 driver=app.config['DB_CONNECTOR'],
                                                                                 user=app.config['DB_USERNAME'],
                                                                                 password=app.config['DB_PASSWORD'],
                                                                                 host=app.config['DB_HOST'],
                                                                                 port=app.config['DB_PORT'],
                                                                                 schema=app.config['DB_DATABASE'])


def stellar_error(json):
    """Converts an error response from the Horizon API to an Error instance that can be thrown.
    
    For example, see: https://www.stellar.org/developers/horizon/reference/errors/transaction-failed.html
    
    :param json:
    :return:
    """
    json = dict(json)
    title = json.get('title', None)
    link = json.get('type', None)
    status = json.get('status', 400)
    message = json.get('detail', None)
    extras = []
    if 'result_codes' in json['extras'] and 'operations' in json['extras']['result_codes']:
        for entry in json['extras']['result_codes']['operations']:
            extras.append(entry)
    return StellarError(title=title, type=link, code=status, message=message, extras=extras)


def is_stellar_error_response(json):
    """Checks the response mapping to see if it matches the structure of an error response from Horizon.
    
    :param json:
    :return:
    """
    return 'title' in json and 'type' in json and 'status' in json and 'extras' in json


def check_account_using_account_id(account_id):
    """Gets the keypair for the account, and confirms that the account exists on the network.
    
    :param account_id:
    :return:
    """
    keypair = Keypair.from_address(account_id)
    # get the keypair for the account id
    account = get_horizon_instance().account(keypair.address())
    # get the account from the network
    if is_stellar_error_response(account):
        raise stellar_error(account)
    return keypair


def check_account_using_seed(seed):
    """Similar to check_account_using_account_id() except this one does the same thing using the seed.
    
    :param seed:
    :return:
    """
    keypair = Keypair.from_seed(seed)
    # gets the keypair for the seed
    account = get_horizon_instance().account(keypair.address())
    # get the account from the network
    if is_stellar_error_response(account):
        raise stellar_error(account)
    return keypair


def get_control_account_seed():
    """Returns the configured base/control account seed to Kinetic.
    
    :return:
    """
    control_account_seed = run.app.config.get('STELLAR_KINETIC_ACCOUNT_SEED', None)
    # we try to get the configured Kinetic control account seed
    if control_account_seed is None:
        raise RuntimeError(message='Configuration error; the control account seed for Kinetic has not been set.')
    return control_account_seed


def get_horizon_instance():
    """Returns the instance of Horizon to be used, based on the current app configuration.
    
    :return:
    """
    stellar_env = run.app.config.get('STELLAR_ENV', 'test')
    # the environment we're running in
    if stellar_env == 'test':
        return horizon_testnet()
    return horizon_livenet()


def is_stellar_in_test():
    """Determines whether or not the current environment is configured to be in test mode.
    
    """
    return run.app.config.get('STELLAR_ENV', 'test') == 'test'


