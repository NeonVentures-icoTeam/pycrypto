class InvalidInputError(ValueError):
    """Exception to be raised whn the user provides some invalid data in the request.
    
    """
    def __init__(self, message, code=400, title='Client Error'):
        super(ValueError, self).__init__()
        self.message = message
        self.code = code
        self.title = title
        self.status = 'error'


class StellarError(RuntimeError):
    """Encapsulates the data returned in an error response from Horizon.
    
    Presents the following attributes:
        - title: short descriptive text of the error
        - code: the HTTP status code for the error
        - message: a longer descriptive message of the error
        - link: The URL to a page describing the error on the stellar.org website
        - extras: a list [] of error codes matching each of the failed operations
    """
    def __init__(self, **kwargs):
        super(RuntimeError, self).__init__()
        self.status = 'error'
        self.code = kwargs.get('code', 500)
        self.title = kwargs.get('title', 'Horizon Error')
        self.message = kwargs.get('message', '')
        self.link = kwargs.get('type', '')
        self.extras = kwargs.get('extras', [])