from app.controllers.home import get_account_info
from app.exceptions import InvalidInputError
from ..helpers import is_stellar_in_test, is_stellar_error_response, get_app_config, stellar_error
from stellar_base.builder import Builder
from stellar_base.keypair import Keypair


def create_new_account(source_seed, opening_balance=1.0, network='test', memo='Creating new account'):
    """Creates a new account on the network from the source account.
    
    You should take note that the source account will be charged for the transaction, and you should pass sufficient
    opening balance to the account to cover for future transactions.
    
    :param source_seed:
    :param opening_balance:
    :param network:
    :param memo:
    :return:
    """
    account_keypair = Keypair.random()
    # generate a keypair for the new account
    builder = Builder(secret=source_seed, network=network)
    # create our transaction builder instance
    opening_balance = float(opening_balance)
    # convert it to a floating point value first
    if opening_balance < 1.0:
        # seems a little low
        raise ValueError('You should fund this account with at least 1 XLM')
    builder.append_create_account_op(account_keypair.address(), str(opening_balance), 'XLM')
    # set the operation
    builder.add_text_memo(memo)
    # add the memo
    builder.sign()
    # sign the transaction for sending to the network
    response = builder.submit()
    # submit the transaction
    if is_stellar_error_response(response):
        raise stellar_error(response)
    return account_keypair


def create_trust_on_account(source_seed, issuer, network='test', asset_code='KINET', limit=None):
    """Adds a trust line for an asset to the network, on an account (source) for the issuer.
    
    :param source_seed:
    :param issuer:
    :param network:
    :param asset_code:
    :param limit:
    :return:
    """
    builder = Builder(secret=source_seed, network=network)
    # create our transaction builder instance
    builder.append_trust_op(destination=issuer, code=asset_code, limit=limit)
    # append the change trust operation
    builder.sign()
    # sign the request
    response = builder.submit()
    # send the transaction to the network
    if is_stellar_error_response(response):
        raise stellar_error(response)
    return response


def create_issuing_account():
    """Creates the issuing account for Kinetic KTC distribution.
    
    :return:
    :raises: StellarError, ValueError
    """
    config = get_app_config()
    # get the current system configuration
    if is_stellar_in_test():
        network = 'test'
    else:
        network = 'public'
    source_seed = config.get('STELLAR_KINETIC_ACCOUNT_SEED', None)
    if source_seed is None or len(source_seed) != 56:
        raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                         '"STELLAR_KINETIC_ACCOUNT_SEED" option MUST be set in the .env file.')
    return create_new_account(source_seed=source_seed, opening_balance=5.0, network=network,
                              memo='Create issuing account')


def create_distribution_account():
    """Creates the distribution account for Kinetic KTC distribution.
    
    :return:
    :raises: StellarError, ValueError
    """
    config = get_app_config()
    # get the current system configuration
    if is_stellar_in_test():
        network = 'test'
    else:
        network = 'public'
    source_seed = config.get('STELLAR_KINETIC_ACCOUNT_SEED', None)
    if source_seed is None or len(source_seed) != 56:
        raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                         '"STELLAR_KINETIC_ACCOUNT_SEED" option MUST be set in the .env file.')
    return create_new_account(source_seed=source_seed, opening_balance=5.0, network=network,
                              memo='Create distribution account')


def create_escrow_holding():
    """Creates the escrow holding account for dealing with loans.

    :return:
    """
    config = get_app_config()
    # get the current system configuration
    if is_stellar_in_test():
        network = 'test'
    else:
        network = 'public'
    source_seed = config.get('STELLAR_KINETIC_ACCOUNT_SEED', None)
    if source_seed is None or len(source_seed) != 56:
        raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                         '"STELLAR_KINETIC_ACCOUNT_SEED" option MUST be set in the .env file.')
    return create_new_account(source_seed=source_seed, opening_balance=2.0, network=network,
                              memo='Create escrow account')


def send_assets_to_account(source_seed, destination, issuer=None, network='test', asset_code='KINET',
                           amount=5000):
    """Creates the assets in the distribution account.
    
    :param source_seed:
    :param destination:
    :param issuer:
    :param network:
    :param asset_code:
    :param amount:
    :return:
    """
    print('Sending {0} {1} to Destination: {2}'.format(asset_code, amount, destination))
    if amount is None:
        raise ValueError('You need to pass a valid integer value for the amount to be paid out.')
    builder = Builder(secret=source_seed, network=network)
    # create our transaction builder instance
    builder.append_payment_op(destination=destination, amount=amount, asset_type=asset_code, asset_issuer=issuer)
    # append the payment operation
    builder.sign()
    # sign the request
    response = builder.submit()
    # send the transaction to the network
    if is_stellar_error_response(response):
        raise stellar_error(response)
    return response


def lock_token_supply(issuer_seed, network='test'):
    """Locks down the issuing account of the token to guarantee that no more tokens will be minted.
    
    This generates trust that the values of the tokens will not be inflated.
    
    :param issuer_seed:
    :param network:
    :return:
    """
    if issuer_seed is None:
        raise ValueError('You need to pass a secret seed for the issuing account.')
    builder = Builder(issuer_seed, network=network)
    # create the transaction builder
    builder.append_set_options_op(master_weight=0, low_threshold=0, med_threshold=0, high_threshold=0)
    # set the options to lock down the issuing account
    builder.sign()
    # sign the transaction
    response = builder.submit()
    # send the transaction to the network
    if is_stellar_error_response(response):
        raise stellar_error(response)
    return response


def set_home_domain_for_asset(source_seed, url, network='test'):
    """Sets the home domain where the stellar.toml file for the asset can be found.
    
    :param source_seed:
    :param url:
    :param network:
    :return:
    """
    str_url = str(url)
    # we forcefully convert it to a string
    if url is None or len(str_url) == 0 or not str_url.startswith('http'):
        raise ValueError('A valid URL needs to start with either "http" or "https')
    builder = Builder(secret=source_seed, network=network)
    # instantiate the transaction builder
    builder.append_set_options_op(home_domain=str_url)
    # set the home domain option
    builder.sign()
    # sign the request
    response = builder.submit()
    # send the transaction to the network
    if is_stellar_error_response(response):
        raise stellar_error(response)
    return response


def create_sale_offer(source_seed, selling_code, selling_issuer, buying_code, buying_issuer=None, price=1,
                      quantity=1000, network='test', raise_exception_on_failure=False):
    """Creates an offer for the asset on the exchange.
    
    :param source_seed:
    :param selling_code:
    :param selling_issuer:
    :param buying_code:
    :param buying_issuer:
    :param price:
    :param quantity:
    :param network:
    :param raise_exception_on_failure:
    :return:
    """
    if str(buying_code).lower() != 'xlm' and buying_issuer is None:
        raise ValueError('You need to set an issuer for the buying currency when it is not Lumens (XLM)')
    builder = Builder(secret=source_seed, network=network)
    # we create the transaction builder
    builder.append_manage_offer_op(selling_code=selling_code, selling_issuer=selling_issuer, buying_code=buying_code,
                                   buying_issuer=buying_issuer, amount=quantity, price=price)
    # create the offer on the exchange
    builder.sign()
    # sign the transaction
    response = builder.submit()
    # send the transaction to the network
    if is_stellar_error_response(response) and raise_exception_on_failure:
        raise stellar_error(response)
    return response


def set_trust_if_required(source_seed, issuer, network='test', currency='KINET', limit=None):
    """Sets trust line on an account, if the account does not already trust the issuer.

    :param source_seed:
    :param issuer:
    :param network:
    :param currency:
    :param limit:
    :return:
    """
    keypair = Keypair.from_seed(source_seed)
    # infer the details from the secret
    account_information = get_account_info(keypair.address(), True)
    # get the account information -- we need to check if this account already has trust for the issuer

    if 'balances' not in account_information:
        raise RuntimeError('We could not determine the state of the destination account. Please try again.')

    if issuer is None:
        raise InvalidInputError('You need to pass the issuer parameter')

    trusts_issuer = False
    for balance in account_information['balances']:
        # check each of the balances to confirm if there is trust
        if balance['asset_type'] == 'native':
            continue
        if balance['asset_code'] == currency and balance['asset_issuer'] == issuer:
            trusts_issuer = True
            break
    if not trusts_issuer:
        # address does not yet trust the issuer, so we create a trust line for the asset
        trust_line = create_trust_on_account(source_seed=source_seed, network=network, issuer=issuer, asset_code=currency,
                                             limit=limit)
        if is_stellar_error_response(trust_line):
            raise stellar_error(trust_line)
    return True

