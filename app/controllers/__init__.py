from flask import Blueprint
import assets
import home
import contracts


routes = []
# routes definition list
controllers = Blueprint('controllers', __name__)
# controller blueprint

routes.append(dict(rule='/', view_func=home.home, options=dict(methods=['GET'])))
routes.append(dict(rule='/generate', view_func=home.register_keys, options=dict(methods=['POST'])))
routes.append(dict(rule='/recover-key', view_func=home.retrieve_key, options=dict(methods=['POST'])))
routes.append(dict(rule='/create-account', view_func=home.create_account, options=dict(methods=['POST'])))
routes.append(dict(rule='/account-info/<account_id>', view_func=home.get_account_info, options=dict(methods=['GET'])))

routes.append(dict(rule='/lend-to', view_func=contracts.create_lending_contract, options=dict(methods=['POST'])))
routes.append(dict(rule='/lend-to/<loan_uuid>/start-release', view_func=contracts.create_release_escrow_for_loan,
                   options=dict(methods=['POST'])))
routes.append(dict(rule='/lend-to/<lending_uuid>/release', view_func=contracts.release_contract,
                   options=dict(methods=['POST'])))
routes.append(dict(rule='/lend-to/<loan_uuid>/complete-release', view_func=contracts.complete_contract_release,
                   options=dict(methods=['POST'])))
routes.append(dict(rule='/lend-to/<escrow_seed>/return', view_func=contracts.return_funds,
                   options=dict(methods=['POST'])))

routes.append(dict(rule='/setup-ktc/create-issuer', view_func=assets.setup_ktc_issuer, options=dict(methods=['POST'])))
routes.append(dict(rule='/setup-ktc/create-distribution', view_func=assets.setup_ktc_distribution,
                   options=dict(methods=['POST'])))
routes.append(dict(rule='/setup-ktc/create-escrow-account', view_func=assets.setup_ktc_escrow_holding,
                   options=dict(methods=['POST'])))
routes.append(dict(rule='/setup-ktc/create-trust', view_func=assets.set_trust_on_distribution,
                   options=dict(methods=['POST'])))
routes.append(dict(rule='/setup-ktc/issue-assets', view_func=assets.create_ktc_assets, options=dict(methods=['POST'])))
routes.append(dict(rule='/setup-ktc/lock-issuer', view_func=assets.lock_issuer, options=dict(methods=['POST'])))
routes.append(dict(rule='/setup-ktc/set-home', view_func=assets.set_home_domain, options=dict(methods=['POST'])))
routes.append(dict(rule='/setup-ktc/create-offer', view_func=assets.create_offer, options=dict(methods=['POST'])))

routes.append(dict(rule='/fund-account', view_func=assets.fund_account, options=dict(methods=['POST'])))

for r in routes:
    # add the defined routes
    controllers.add_url_rule(r['rule'], endpoint=r.get('endpoint', None), view_func=r['view_func'],
                             **r.get('options', {}))

