from flask import jsonify, request
from stellar_base.keypair import Keypair

from ..exceptions import StellarError, InvalidInputError
from ..kinetic import create_issuing_account, create_distribution_account, \
    send_assets_to_account, create_trust_on_account, create_sale_offer, lock_token_supply, \
    set_home_domain_for_asset, create_escrow_holding, set_trust_if_required
from ..helpers import get_app_config, is_stellar_in_test


def setup_ktc_issuer():
    """Sets up the issuing account for the KTC tokens.
    
    :return:
    """
    try:
        keypair = create_issuing_account()
        return jsonify(dict(public_key=keypair.address(), seed=keypair.seed()))
        # return the response
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def setup_ktc_distribution():
    """Sets up the distribution account for the KTC tokens.
    
    :return:
    """
    try:
        keypair = create_distribution_account()
        return jsonify(dict(public_key=keypair.address(), seed=keypair.seed()))
        # return the response
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def setup_ktc_escrow_holding():
    """Sets up the escrow holding account for holding KINETs from loans.

    :return:
    """
    try:
        keypair = create_escrow_holding()
        return jsonify(dict(public_key=keypair.address(), seed=keypair.seed()))
        # return the response
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def set_trust_on_distribution():
    """Adds a trust line on the distribution account.
    
    :return:
    """
    try:
        if request.json is None:
            raise InvalidInputError('You did not supply any JSON data in the request.')
        config = get_app_config()
        # get the configuration
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'
        issuer_address = config.get('KINETIC_KINET_ISSUER_ADDRESS', None)
        distribution_seed = config.get('KINETIC_KINET_DISTRIBUTION_SEED', None)
        # get the request data
        if issuer_address is None or distribution_seed is None:
            raise InvalidInputError('You need to both the "KINETIC_KINET_ISSUER_ADDRESS", and ' +
                                    '"KINETIC_KINET_DISTRIBUTION_SEED" in the .env file')
        trust_line = create_trust_on_account(source_seed=distribution_seed, issuer=issuer_address,
                                             network=network, asset_code=config.get('KINETIC_KTC_CODE', 'KINET'),
                                             limit=config.get('KINETIC_KTC_LIMIT', None))
        return jsonify(trust_line)
        # return the response
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def create_ktc_assets():
    """Issue the number of KTC tokens to the distribution account from the specified issuer.
    
    :return:
    """
    try:
        if request.json is None:
            raise InvalidInputError('You did not supply any JSON data in the request.')
        config = get_app_config()
        # get the configuration
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'
        issuer_seed = config.get('KINETIC_KINET_ISSUER_SEED', None)
        distribution_address = config.get('KINETIC_KINET_DISTRIBUTION_ADDRESS', None)
        # get the request data
        if issuer_seed is None or distribution_address is None:
            raise InvalidInputError('You need to both the "KINETIC_KINET_ISSUER_SEED", and ' +
                                    '"KINETIC_KINET_DISTRIBUTION_ADDRESS" in the .env file')
        issuing_account_keypair = Keypair.from_seed(issuer_seed)
        # get the keypair from the seed
        payment = send_assets_to_account(source_seed=issuing_account_keypair.seed(),
                                         destination=distribution_address,
                                         issuer=issuing_account_keypair.address(),
                                         network=network,
                                         asset_code=config.get('KINETIC_KTC_CODE', 'KINET'),
                                         amount=config.get('KINETIC_KTC_LIMIT', None))
        return jsonify(payment)
        # return the response
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def set_home_domain():
    """Sets the home domain for our KTC asset, where the stellar.toml file can be referenced.
    
    :return:
    """
    try:
        if request.json is None:
            raise InvalidInputError('You did not supply any JSON data in the request.')
        config = get_app_config()
        # get the configuration
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'
        issuer_seed = config.get('KINETIC_KINET_ISSUER_SEED', None)
        # get the request data
        if issuer_seed is None:
            raise InvalidInputError('You need to set the "KINETIC_KINET_ISSUER_SEED" in the .env file')
        home_domain = set_home_domain_for_asset(source_seed=issuer_seed,
                                                url=config.get('KINETIC_KTC_HOME_DOMAIN', None), network=network)
        # set the home domain for the asset
        return jsonify(home_domain)
        # return the response
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def lock_issuer():
    """Locks down issuance of KTC tokens from the issuing account.

    :return:
    """
    try:
        if request.json is None:
            raise InvalidInputError('You did not supply any JSON data in the request.')
        config = get_app_config()
        # get the configuration
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'
        issuer_seed = config.get('KINETIC_KINET_ISSUER_SEED', None)
        # get the request data
        if issuer_seed is None:
            raise InvalidInputError('You need to set the "KINETIC_KINET_ISSUER_SEED" in the .env file')
        lock_down = lock_token_supply(issuer_seed=issuer_seed, network=network)
        # send the lock down request
        return jsonify(lock_down)
        # return the response
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def create_offer():
    """Creates the sale offer for our newly minted KTC token.
    
    This also sets up the exchange rate for our tokens on the exchange.
    
    :return:
    """
    try:
        if request.json is None:
            raise InvalidInputError('You did not supply any JSON data in the request.')
        config = get_app_config()
        # get the configuration
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'
        issuer_address = config.get('KINETIC_KINET_ISSUER_ADDRESS', None)
        distribution_seed = config.get('KINETIC_KINET_DISTRIBUTION_SEED', None)
        # get the request data
        if issuer_address is None or distribution_seed is None:
            raise InvalidInputError('You need to both the "KINETIC_KINET_ISSUER_ADDRESS", and ' +
                                    '"KINETIC_KINET_DISTRIBUTION_SEED" in the .env file')
        offer = create_sale_offer(source_seed=distribution_seed,
                                  selling_code=config.get('KINETIC_KTC_CODE', 'KINET'),
                                  selling_issuer=issuer_address,
                                  buying_code=config.get('KINETIC_KTC_CODE_BUYING', 'XLM'),
                                  buying_issuer=config.get('KINETIC_KTC_CODE_BUYING_ISSUER', None),
                                  quantity=config.get('KINETIC_KTC_SALE'),
                                  price=config.get('KINETIC_KTC_CODE_BUYING_PRICE', 1), network=network)
        # send the transaction to the network - it's not critical if it fails or succeeds
        return jsonify(offer)
        # return the response
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def fund_account():
    """Issues a currency to an account. It currently supports only Lumens (XLM), and KINETs.

    :return:
    """
    allowed_currencies = ['XLM', 'KINET']
    try:
        if request.json is None:
            raise InvalidInputError('You did not supply any JSON data in the request.')
        config = get_app_config()
        # get the configuration
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'

        destination = request.json.get('address', None)
        destination_seed = request.json.get('destination_seed', None)
        amount = request.json.get('amount', None)
        currency = request.json.get('currency', None)
        # get the supplied funding currency

        if currency is None or currency.upper() not in allowed_currencies:
            raise ValueError('The currency needs to be one of: {0}'.format(', '.join(allowed_currencies)))
        if amount is None:
            raise ValueError('You need to pass the value to be credited to the address')
        if destination_seed is None and destination is None:
            raise InvalidInputError(
                'You need to pass either the "destination_seed", or "address" to be credited in the request'
            )

        if destination_seed is not None:
            destination_keypair = Keypair.from_seed(destination_seed)
            # get the destination details from the seed
            destination = destination_keypair.address()
        print('Destination From Seed: {0}'.format(destination))
        amount = float(amount)
        # we cast it, and throw any possible error
        source_seed = config.get('STELLAR_KINETIC_ACCOUNT_SEED', None)
        distribution_seed = config.get('KINETIC_KINET_DISTRIBUTION_SEED', None)
        # get setup data
        if currency == 'KINET':
            # we're funding with KINETs
            if distribution_seed is None:
                raise InvalidInputError('You need to set the "KINETIC_KINET_DISTRIBUTION_SEED" in the .env file')
            issuer_address = config.get('KINETIC_KINET_ISSUER_ADDRESS', None)
            # get setup data
            set_trust_if_required(source_seed=destination_seed, issuer=issuer_address, network=network,
                                  currency=currency, limit=config.get('KINETIC_KTC_LIMIT', None))
            # set trust on the account, if required
            payment = send_assets_to_account(source_seed=distribution_seed, destination=destination,
                                             issuer=issuer_address, network=network, asset_code=currency,
                                             amount=amount)
        else:
            # we're funding with Lumens
            if source_seed is None:
                raise InvalidInputError('You need to both the "STELLAR_KINETIC_ACCOUNT_SEED" in the .env file')
            payment = send_assets_to_account(source_seed=source_seed, destination=destination,
                                             network=network, asset_code=currency, amount=amount)
        return jsonify(payment)
        # return the response
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]

