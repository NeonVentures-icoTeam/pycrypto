from flask import jsonify, request
from stellar_base.builder import Builder
from stellar_base.keypair import Keypair

import run
from ..exceptions import InvalidInputError, StellarError
from ..helpers import stellar_error, is_stellar_error_response, is_stellar_in_test, check_account_using_seed, \
    get_app_config
from ..kinetic import send_assets_to_account, create_trust_on_account, set_trust_if_required


def create_escrow_account_from_source(source_seed, starting_balance=1.0, network='test'):
    """Creates a new account from a base source account, setting the starting balance to the specified value.
    
    The starting balance must account for the signing fees for all signers: 0.5 * number_of_signers
    See: https://www.stellar.org/developers/guides/concepts/fees.html#minimum-account-balance
    
    :param source_seed:
    :param starting_balance:
    :param network:
    :return:
    """
    escrow_keypair = Keypair.random()
    # generate a keypair for the new account
    builder = Builder(secret=source_seed, network=network)
    # create our transaction builder instance
    builder.append_create_account_op(escrow_keypair.address(), str(starting_balance), 'XLM')
    # set the operation
    builder.add_text_memo('Creating new escrow account')
    # add the memo
    builder.sign()
    # sign the transaction for sending to the network
    response = builder.submit()
    # submit the transaction
    if is_stellar_error_response(response):
        raise stellar_error(response)
    return escrow_keypair.address(), escrow_keypair.seed()


def set_signers_on_escrow(escrow_seed, lender_address=None, network='test'):
    """Sets the signers on the escrow account.

    :param escrow_seed:
    :param lender_address:
    :param network:
    :return:
    """
    config = get_app_config()
    # get the current system configuration
    kinetic_address = config.get('STELLAR_KINETIC_ACCOUNT_ADDRESS', None)
    if kinetic_address is None or len(kinetic_address) != 56:
        raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                         '"STELLAR_KINETIC_ACCOUNT_ADDRESS" option MUST be set in the .env file.')
    builder = Builder(secret=escrow_seed, network=network)
    # create our transaction builder instance
    threshold_weight = 1
    # default threshold weight for signatures
    if lender_address is not None:
        # we have a valid lender address to add as a signer
        threshold_weight = 2
        # we increase it, since we have the lender as a signer as well
        builder.append_set_options_op(signer_address=lender_address, signer_type='ed25519PublicKey', signer_weight=1)

    builder.append_set_options_op(signer_address=kinetic_address, signer_type='ed25519PublicKey', signer_weight=1)
    # add the signer option for the escrow account - Lender + KineticCredit
    builder.append_set_options_op(master_weight=0, low_threshold=threshold_weight, med_threshold=threshold_weight,
                                  high_threshold=threshold_weight)
    # add the operation for the person doing the lending
    builder.add_text_memo('Adding the signing parties')
    # set text memo
    builder.sign()
    # sign the transaction
    response = builder.submit()
    # send it to the network
    if is_stellar_error_response(response):
        raise stellar_error(response)
    return response


def create_lending_contract():
    """Starts an escrow lending flow for the from the specified source_account (as per the seed in the request), to the
    destination account from the arguments list to this function.
    
    It does the following things:
        - Creates an escrow account
        - Funds it with the desired amount to be borrowed + (0.5 * 2) -- to cover transaction charges for signers
        - Adjusts the weights of the different signers on the contract
    
    The return value is in the form:
    {
        transaction: horizon_response_for_escrow_smart_contract,
        escrow_account: {account_id: escrow_account_id, seed: escrow_secret_seed}
    }
    
    horizon_response_for_Escrow_smart_contract: See the response here -
    https://horizon-testnet.stellar.org/accounts/GAFMVD63P3ZQB7AX6M2ZNWSRMC7HMXWEVQQNRSZDXJMT754AHDKZMH6N

    :return:
    """
    try:
        config = get_app_config()
        # get the current system configuration
        kinet_issuer = config.get('KINETIC_KINET_ISSUER_ADDRESS', None)
        if kinet_issuer is None or len(kinet_issuer) != 56:
            raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                             '"KINETIC_KINET_ISSUER_ADDRESS" option MUST be set in the .env file.')
        lender_seed = request.json.get('seed', None)
        # get the seed from the request
        amount = request.json.get('amount', None)
        # get the amount to fund for
        if lender_seed is None or amount is None:
            raise InvalidInputError('Both the seed, and amount to be funded need to be provided in the request')
        lender_keypair = check_account_using_seed(lender_seed)
        # gets the keypair for the lending account
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'
        amount = float(amount)
        # we parse it to a floating point value
        lumens_amount = 1 + (0.5 * 3)
        # adding the transaction fee for all signers
        escrow_pubkey, escrow_seed = create_escrow_account_from_source(source_seed=lender_seed,
                                                                       starting_balance=lumens_amount, network=network)
        # generate a keypair for the new account, and sets the starting balance
        create_trust_on_account(source_seed=escrow_seed, network=network, issuer=kinet_issuer,
                                asset_code=config.get('KINETIC_KTC_CODE', 'KINET'),
                                limit=config.get('KINETIC_KTC_LIMIT', None))
        set_signers = set_signers_on_escrow(escrow_seed=escrow_seed, lender_address=None, network=network)
        # send it to the network
        transfer = send_assets_to_account(source_seed=lender_seed, destination=escrow_pubkey, issuer=kinet_issuer,
                                          network=network, amount=amount,
                                          asset_code=config.get('KINETIC_KTC_CODE', 'KINET'))
        # transfer the funds to escrow
        if is_stellar_error_response(transfer):
            raise stellar_error(transfer)
        return jsonify(dict(signers=set_signers, escrow_account=dict(account_id=escrow_pubkey, seed=escrow_seed),
                            transfer=transfer, lender_address=lender_keypair.address()))
    
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def create_release_escrow_for_loan(loan_uuid):
    """Creates an escrow account to collect together all funds lent to a borrower together first, before further
    distribution.

    :param loan_uuid:
    :return:
    """
    try:
        loan = run.Loan.query.filter_by(uuid=loan_uuid).first()
        # import the Loan model
        config = get_app_config()
        # get the current system configuration
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'
        kinetic_seed = config.get('STELLAR_KINETIC_ACCOUNT_SEED', None)
        if kinetic_seed is None:
            raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                             '"STELLAR_KINETIC_ACCOUNT_SEED" option MUST be set in the .env file.')

        kinet_issuer = config.get('KINETIC_KINET_ISSUER_ADDRESS', None)
        if kinet_issuer is None or len(kinet_issuer) != 56:
            raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                             '"KINETIC_KINET_ISSUER_ADDRESS" option MUST be set in the .env file.')

        escrow_pubkey, escrow_seed = create_escrow_account_from_source(source_seed=kinetic_seed, starting_balance=1.5,
                                                                       network=network)
        # generate a keypair for the new account, and sets the starting balance
        set_trust_if_required(source_seed=escrow_seed, issuer=kinet_issuer, network=network,
                              currency=config.get('KINETIC_KTC_CODE', 'KINET'),
                              limit=loan.amount)
        # set trust on the account, if required - to a limit of the max KINETs
        return jsonify(dict(pubkey=escrow_pubkey, seed=escrow_seed))
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def release_contract(lending_uuid):
    """Releases the funds to the collection escrow account from the lender.
    
    :param lending_uuid:
    :return:
    """
    try:
        lender = run.Lender.query.filter_by(uuid=lending_uuid).first()
        # get all the lenders for the loan
        config = get_app_config()
        # get the current system configuration
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'
        kinetic_seed = config.get('STELLAR_KINETIC_ACCOUNT_SEED', None)
        if kinetic_seed is None:
            raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                             '"STELLAR_KINETIC_ACCOUNT_SEED" option MUST be set in the .env file.')

        kinet_issuer = config.get('KINETIC_KINET_ISSUER_ADDRESS', None)
        if kinet_issuer is None or len(kinet_issuer) != 56:
            raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                             '"KINETIC_KINET_ISSUER_ADDRESS" option MUST be set in the .env file.')

        collection_address = request.json.get('collection_address', None)
        # get the collection account address from the request
        amount = lender.pledge
        # get the amount to move from the escrow account
        if collection_address is None or amount is None:
            raise InvalidInputError('You need to pass the collection_address for the loan in the request')

        builder = Builder(secret=lender.contract_data.escrow_seed, network=network)
        # create our transaction builder instance
        builder.append_payment_op(destination=collection_address, amount=amount,
                                  asset_type=config.get('KINETIC_KTC_CODE', 'KINET'), asset_issuer=kinet_issuer)
        # append the payment transaction for the agreed upon amount
        builder.sign(secret=kinetic_seed)
        # sign the request
        response = builder.submit()
        # send it to the network
        if is_stellar_error_response(response):
            raise stellar_error(response)
        return jsonify(response)
        
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def complete_contract_release(loan_uuid):
    """Checks the loan, and moves the funds to the appropriate addresses.

    :param loan_uuid:
    :return:
    """
    try:
        loan = run.Loan.query.filter_by(uuid=loan_uuid).first()
        # import the Loan model
        config = get_app_config()
        # get the current system configuration
        collection_seed = request.json.get('collection_seed', None)
        # get the collection account seed from the request
        if collection_seed is None:
            raise InvalidInputError('You need to pass the collection_seed for the loan in the request')
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'

        kinetic_escrow_address = config.get('KINETIC_KINET_ESCROW_ADDRESS', None)
        if kinetic_escrow_address is None:
            raise ValueError('The Kinetic Credit escrow address was not properly configured. The '
                             '"KINETIC_KINET_ESCROW_ADDRESS" option MUST be set in the .env file.')

        kinet_issuer = config.get('KINETIC_KINET_ISSUER_ADDRESS', None)
        if kinet_issuer is None or len(kinet_issuer) != 56:
            raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                             '"KINETIC_KINET_ISSUER_ADDRESS" option MUST be set in the .env file.')

        distribution_address = config.get('KINETIC_KINET_DISTRIBUTION_ADDRESS', None)
        # get the request data
        if distribution_address is None:
            raise InvalidInputError('You need to both the "KINETIC_KINET_DISTRIBUTION_ADDRESS" in the .env file')

        total_amount = loan.amount
        holding_amount = round(total_amount / 10.0, 7)
        arrangement_fee = round(total_amount * 0.02, 7)
        non_refundable_amount = round(total_amount * 0.025, 7)
        effective_amount = total_amount - (holding_amount + arrangement_fee + non_refundable_amount)
        # the amounts and charges
        borrower_address = request.json.get('borrower_address', None)
        # get the borrower account address from the request

        transfer_destinations = {borrower_address: {'amount': effective_amount, 'name': 'borrower_transfer'},
                                 kinetic_escrow_address: {'amount': holding_amount, 'name': 'holding_transfer'},
                                 distribution_address: {'amount': non_refundable_amount + arrangement_fee,
                                                        'name': 'non_refundable_transfer'}}
        # the amounts to transfer
        transfers = dict()

        for address, transfer in transfer_destinations.items():
            builder = Builder(secret=collection_seed, network=network)
            # create our transaction builder instance
            builder.append_payment_op(destination=address, amount=transfer['amount'],
                                      asset_type=config.get('KINETIC_KTC_CODE', 'KINET'), asset_issuer=kinet_issuer)
            # append the payment transaction for the agreed upon amount
            builder.sign()
            # sign the request
            transaction = builder.submit()
            # send it to the network
            if is_stellar_error_response(transaction):
                e = stellar_error(transaction)
                transfers[transfer['name']] = dict(status=e.status, reason=e.message, title=e.title, link=e.link,
                                                   extras=e.extras)
            else:
                transfers[transfer['name']] = transaction

        return jsonify(transfers)
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]


def return_funds(escrow_seed):
    """Returns the funds (KINETs) back to the lender, since the entire contract was cancelled.

    :param escrow_seed:
    :return:
    """
    try:
        config = get_app_config()
        # get the current system configuration
        kinetic_seed = config.get('STELLAR_KINETIC_ACCOUNT_SEED', None)
        if kinetic_seed is None:
            raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                             '"STELLAR_KINETIC_ACCOUNT_SEED" option MUST be set in the .env file.')
        kinet_issuer = config.get('KINETIC_KINET_ISSUER_ADDRESS', None)
        if kinet_issuer is None or len(kinet_issuer) != 56:
            raise ValueError('The Kinetic Credit secret seed was not properly configured. The '
                             '"KINETIC_KINET_ISSUER_ADDRESS" option MUST be set in the .env file.')

        lender_address = request.json.get('lender_address', None)
        # get the lender account address from the request
        amount = request.json.get('amount', None)
        # get the amount to fund for
        if lender_address is None or amount is None:
            raise InvalidInputError('You need to pass the lender_address, and amount in the request')
        amount = float(amount)
        if is_stellar_in_test():
            network = 'test'
        else:
            network = 'public'
        builder = Builder(secret=escrow_seed, network=network)
        # create our transaction builder instance
        builder.append_payment_op(destination=lender_address, amount=amount,
                                  asset_type=config.get('KINETIC_KTC_CODE', 'KINET'), asset_issuer=kinet_issuer)
        # append the payment transaction for the agreed upon amount
        builder.sign(secret=kinetic_seed)
        # sign the request
        response = builder.submit()
        # send it to the network
        if is_stellar_error_response(response):
            raise stellar_error(response)
        return jsonify(response)

    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Bad Input'), 400
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    return jsonify(response[0]), response[1]

