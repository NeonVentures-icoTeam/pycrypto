from flask import jsonify, request
import requests
import run
from stellar_base.builder import Builder
from stellar_base.keypair import Keypair
from stellar_base.utils import StellarMnemonic
from ..exceptions import InvalidInputError, StellarError
from ..helpers import stellar_error, is_stellar_error_response, get_horizon_instance, is_stellar_in_test, get_app_config


def home():
    """Our index (/) controller
    
    :return:
    """
    return 'Welcome to PyCrypto by Neon :)'


def register_keys():
    """Creates a new keypair for use by the requester.
    
    :return:
    """
    mnemonic = (StellarMnemonic()).generate()
    # generates a recovery mnemonic for the user
    keypair = Keypair.deterministic(mnemonic)
    # generate the key pair
    return jsonify(dict(public_key=keypair.address().decode(), seed=keypair.seed().decode(), recovery_phrase=mnemonic))


def retrieve_key():
    """Performs a recovery for the keypair based on the values sent in the request.
    
    Retrieval can either be done by passing the seed, or the mnemonic previously used to generate the keypair.
    
    :return:
    """
    try:
        if request.json is None:
            raise IOError('You need to pass at least the phrase in the request.')
        mode = request.json.get('mode', 'seed')
        phrase = request.json.get('phrase', '')
        # get the data passed in the request
        if len(phrase) == 0:
            raise InvalidInputError('You passed an empty phrase in the request. This does not seem right!')
        if mode == 'recovery_phrase':
            # generate it from a mnemonic
            keypair = Keypair.deterministic(phrase)
            response = dict(recovery_phrase=phrase)
        else:
            # generate it from the seed
            keypair = Keypair.from_seed(phrase)
            response = dict(seed=keypair.seed().decode())
        response['public_key'] = keypair.address().decode()
        return jsonify(response)
    
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.status
    return jsonify(response[0]), response[1]
    

def create_account():
    """Creates a new account on Horizon, and funds it with a minimum balance of 1XLM (for the live network).
    
    On the test network, friendbot funds it with a large amount of coins :)
    
    :return:
    """
    try:
        config = get_app_config()
        # get the configuration
        server_url = config.get('STELLAR_HORIZON_SERVER', 'https://horizon-testnet.stellar.org')
        # we get the server URL provided in the config
        account_id = request.json.get('account_id', None)
        account_seed = request.json.get('account_seed', None)
        amount = request.json.get('amount', 1)
        # gets the account_id from the request
        if account_id is None and account_seed is None:
            raise InvalidInputError('You did not provide the account_id, or account_seed. You should generate one first.')
        if account_seed is not None:
            account_keypair = Keypair.from_seed(account_seed)
            # get the account details from the seed
            account_id = account_keypair.address()
            
        if is_stellar_in_test():
            response = requests.get(server_url + '/friendbot', params={'addr': account_id})
        else:
            control_account_seed = run.app.config.get('STELLAR_KINETIC_ACCOUNT_SEED', None)
            # we try to get the configured Kinetic control account seed
            if control_account_seed is None:
                raise RuntimeError(message='Configuration error; the control account seed for Kinetic has not been set.')
            builder = Builder(secret=control_account_seed, network='public')
            # create our transaction builder instance
            builder.append_create_account_op(account_id, amount, 'XLM')
            # set the operation
            builder.add_text_memo('Creating new customer account')
            # add the memo
            builder.sign()
            # sign the transaction for sending to the network
            response = builder.submit()
            # submit the transaction
        if is_stellar_error_response(response.json()):
            raise stellar_error(response.json())
        return jsonify(response.json())
    
    except InvalidInputError as e:
        response = dict(status=e.status, reason=e.message, title=e.title), e.code
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except ValueError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    except RuntimeError as e:
        response = dict(status='error', reason=e.message, title='Something went wrong'), 500
    except Exception as e:
        response = dict(status='error', reason=e.message, title='Configuration Error'), 500
    return jsonify(response[0]), response[1]


def get_account_info(account_id, return_raw=False):
    """Retrieves information about an account on the network.

    :param account_id:
    :param return_raw:
    :return:
    """
    try:
        horizon = get_horizon_instance()
        # get our Horizon instance
        response = horizon.account(account_id)
        if is_stellar_error_response(response):
            raise stellar_error(response)
        if return_raw:
            # return it as a dictionary
            return response
        return jsonify(response)
    except StellarError as e:
        response = dict(status=e.status, reason=e.message, title=e.title, link=e.link, extras=e.extras), e.code
    except Exception as e:
        response = dict(status='error', reason=e.message, title='Configuration Error'), 500
    return jsonify(response[0]), response[1]

