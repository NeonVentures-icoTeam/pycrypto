#!/usr/bin/env python
import multiprocessing
import os.path


bind = "127.0.0.1:8080"
workers = multiprocessing.cpu_count() * 2 + 1
chdir = os.path.dirname(os.path.abspath(__file__))

