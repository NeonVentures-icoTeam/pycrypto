import bugsnag
from flask import Flask, jsonify
from flask_dotenv import DotEnv
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from app.helpers import get_db_connection_string
from app.controllers import controllers
import logging
from bugsnag.handlers import BugsnagHandler
import os.path

app = Flask(__name__)
# setup our web app
env = DotEnv(app)
# load config values from the .env file
app.config['SQLALCHEMY_DATABASE_URI'] = get_db_connection_string()
# set the database connection string
db = SQLAlchemy(app)
# setup SQLAlchemy
marshmallow = Marshmallow(app)
# setup Marshmallow
project_directory = os.path.dirname(os.path.abspath(__file__))
# we set our project directory
bugsnag.configure(
  api_key="b3ada5f321650b97885fc672a6f9d17e",
  project_root=project_directory,
)
# configure bugsnag
logger = logging.getLogger('PyCrypto')
bugsnagHandler = BugsnagHandler()
bugsnagHandler.setLevel(logging.ERROR)
logger.addHandler(bugsnagHandler)
# add bugsnag as an error logging handler

# Register app Blueprints
app.register_blueprint(controllers)

class Lender(db.Model):
    __tablename__ = 'lenders'
    id = db.Column(db.BigInteger, primary_key=True)
    uuid = db.Column(db.String(50), unique=True)
    loan_id = db.Column(db.BigInteger, nullable=False)
    user_id = db.Column(db.Integer, nullable=False)
    pledge = db.Column(db.DECIMAL(precision=7), nullable=False)
    contract_data = db.Column(db.JSON, nullable=True)
    created_at = db.Column(db.TIMESTAMP)
    updated_at = db.Column(db.TIMESTAMP, nullable=True)

    def __repr__(self):
        return '<Lender %r>' % self.uuid


class Loan(db.Model):
    __tablename__ = 'loans'
    id = db.Column(db.BigInteger, primary_key=True)
    uuid = db.Column(db.String(50), unique=True)
    loan_application_id = db.Column(db.Integer, nullable=False)
    currency = db.Column(db.String(3), nullable=False)
    amount = db.Column(db.DECIMAL(precision=7), nullable=False)
    opens_at = db.Column(db.TIMESTAMP)
    closes_at = db.Column(db.TIMESTAMP)
    contract_data = db.Column(db.JSON, nullable=True)
    created_at = db.Column(db.TIMESTAMP)
    updated_at = db.Column(db.TIMESTAMP, nullable=True)

    def __repr__(self):
        return '<Loan %r>' % self.uuid


@app.errorhandler(400)
def error_400(e):
    if e.message is None or len(e.message) == 0:
        message = 'Please check the documentation, and be certain you are passing all the required data in the request.'
    else:
        message = e.message
    return jsonify(dict(status='error', title='Bad Request', message=message, error=e.message)), 400


@app.errorhandler(405)
def error_405(e):
    return jsonify(dict(status='error', title='Method not Allowed', error=e.message,
                        message='Please check the documentation, and be certain you are making the correct call.')), 405


@app.errorhandler(500)
def error_500(e):
    return jsonify(dict(status='error', title='Internal Server Error', error=e.message,
                        message='Something went wrong while processing your request. Please try again later.')), 500


if __name__ == '__main__':
    app.run(port=8080, debug=False)

